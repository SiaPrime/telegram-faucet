package main

import (
	"gitlab.com/SiaPrime/telegram-faucet/internal/primebot"
)

func main() {
	primebot.Run()
}
