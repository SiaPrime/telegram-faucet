package usage

import "gitlab.com/SiaPrime/telegram-faucet/internal/users"

// Usage holds the path and report
type Usage struct {
	Path   string
	Report *Report
}

// Report holds data on the usage of the bot
type Report struct {
	GroupMembers int
	UserStats    *users.UserStats
	CommandUsage *CommandUsage
}

// CommandUsage contains information on command invocations for the bot
type CommandUsage struct {
	Approve          int
	ApproveAll       int
	Balance          int
	ID               int
	Invite           int
	Refer            int
	Register         int
	Report           int
	Send             int
	Start            int
	StartWithPayload int
	Stats            int
	List             int
	FixReferrals     int
	Dump             int
}
