package usage

import (
	"encoding/json"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

// LoadUsage loads the provided usage file
func (u *Usage) LoadUsage() {
	logrus.Info("Loading usage file.")
	data, err := ioutil.ReadFile(u.Path)
	if err != nil {
		logrus.Errorf("Error: %v", err)
	}
	report := Report{}
	err = json.Unmarshal(data, &report)
	if err != nil {
		logrus.Errorf("Error: %v", err)
	}
	u.Report = &report
}

// StoreUsage stores the provided usage file
func (u *Usage) StoreUsage() {
	logrus.Info("Saving Primebot usage file.")
	data, err := json.Marshal(u.Report)
	if err != nil {
		logrus.Errorf("Error: %v", err)
		return
	}
	err = ioutil.WriteFile(u.Path, data, 0777)
	if err != nil {
		logrus.Errorf("Error: %v", err)
	}
}
