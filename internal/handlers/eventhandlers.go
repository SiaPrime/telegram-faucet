package handlers

import (
	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// OnAddedToGroup logs group details when the bot is added to the group, for configuration purposes
func (hm *HandlerManager) OnAddedToGroup(m *tb.Message) {
	logrus.Infof("Added to group: %s (id: %d)", m.Chat.Title, m.Chat.ID)
}

// OnUserJoined logs group details when the bot is added to the group, for configuration purposes
func (hm *HandlerManager) OnUserJoined(m *tb.Message) {
	if m.Chat.ID == hm.TrackedGroup.ID {
		for _, tbUser := range m.UsersJoined {
			logrus.Infof("%s invited %s", m.Sender.Username, tbUser.Username)
			logrus.Infof("invite extra: %v %v %v", m, m.Sender, tbUser)
			if user, ok := hm.Users.Load(tbUser.ID); ok {
				if user.IsRegistered() && !user.HasJoinReward() {
					if !hm.Disabled {
						hm.sendJoinApprovalMessage(tbUser.ID)
					}
				}
			} else {
				if tbUser.ID == m.Sender.ID {
					return
				}
				hm.Users.CreateWithReferrer(tbUser.ID, m.Sender.ID)
				_, err := hm.Bot.Send(m.Sender, "Your referral @%s has joined the group, now have them message `/start` to me to earn your reward!", tbUser.Username)
				if err != nil {
					logrus.Error(err)
				}
			}
		}
	}
}
