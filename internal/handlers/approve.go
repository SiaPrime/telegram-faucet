package handlers

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/combot"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Approve approves a user for rewards.
func (hm *HandlerManager) Approve(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Approve++
	for _, field := range strings.Fields(m.Payload) {
		userID, err := strconv.Atoi(field)
		if err != nil {
			hm.adminError(fmt.Sprintf("That was an invalid userID (%s). Invalid input.", field))
			logrus.Error(err)
			continue
		}
		hm.approveOne(userID)
	}
}

// ApproveAll approves all pending approvals
func (hm *HandlerManager) ApproveAll(m *tb.Message) {
	hm.Usage.Report.CommandUsage.ApproveAll++
	numApproved := 0
	maxToApprove, err := strconv.Atoi(m.Payload)
	if err != nil {
		maxToApprove = 150
	}
	remainingToApprove := 0
	pendingApprovals := hm.Users.ListApprovals()
	hm.Bot.Send(hm.AdminGroup, fmt.Sprintf("Approving at most %d users out of %d pending approvals.", maxToApprove, len(pendingApprovals)))
	for _, userID := range pendingApprovals {
		if numApproved < maxToApprove {
			hm.approveOne(userID)
			numApproved++
		} else {
			remainingToApprove++
		}
	}
	hm.Bot.Send(hm.AdminGroup, fmt.Sprintf("Approved %d users. %d remaining.", numApproved, remainingToApprove))
}

func (hm *HandlerManager) approveOne(userID int) {
	user, ok := hm.Users.Load(userID)
	if !ok {
		hm.adminError(fmt.Sprintf("That was an invalid userID (%d). No user object.", userID))
		return
	}
	tbUser, err := hm.Bot.ChatMemberOf(hm.TrackedGroup, user.TBUser())
	if err != nil {
		hm.adminError(fmt.Sprintf("That was an invalid userID (%d). Not a member of the group.", userID))
		logrus.Error(err)
		return
	}
	approval := hm.Users.PopApproval(userID)
	if approval == nil {
		hm.adminError(fmt.Sprintf("That user (id: %d) didn't have anything waiting for approval.", userID))
		return
	}
	if approval.Reason == "join" {
		ok = hm.rewardJoin(user.TelegramID, tbUser.User.Username)
		if !ok {
			hm.Bot.Send(hm.AdminGroup, "There was an issue rewarding that user! You can try approving them again after resolving the problem.")
			hm.Users.AddApproval(userID, approval.Reason, approval.Message)
		} else {
			hm.Bot.Send(hm.AdminGroup, fmt.Sprintf("User %s (%d) was rewarded for joining.", tbUser.User.Username, tbUser.User.ID))
		}
	}
}

func getName(user *tb.User) string {
	name := user.Username
	if name == "" {
		name = user.FirstName + " " + user.LastName
	}
	if name == " " {
		name = "<no name>"
	}
	return name
}

func (hm *HandlerManager) sendJoinApprovalMessage(userID int) {
	if hm.Disabled {
		return
	}
	var msg strings.Builder
	user, ok := hm.Users.Load(userID)
	if !ok {
		logrus.Errorf("Attempted to send approval for a user that doesn't exist (id: %d).", userID)
		return
	}
	fullTbUser, _ := hm.Bot.ChatMemberOf(hm.TrackedGroup, user.TBUser())
	username := getName(fullTbUser.User)

	msg.WriteString(fmt.Sprintf("User %s (%d) is requesting approval:", username, userID))
	msg.WriteString(fmt.Sprintf("\n\tFull user data: %+v", *fullTbUser.User))
	msg.WriteString("\n\tReferrer: ")
	if user.HasReferrer() {
		referringUser, _ := hm.Users.Load(user.ReferrerID)
		referrerTbUser, _ := hm.Bot.ChatMemberOf(hm.TrackedGroup, referringUser.TBUser())
		referrerName := getName(referrerTbUser.User)
		msg.WriteString(fmt.Sprintf("%s (%d rewarded referrals)", referrerName, len(referringUser.ReferralRewards)))
		msg.WriteString(fmt.Sprintf("\n\tFull referrer data: %+v", *referrerTbUser.User))
	} else {
		msg.WriteString("NONE")
	}
	msg.WriteString("\n\tCombot status: ")
	check, err := combot.Check(userID)
	if err != nil {
		msg.WriteString("There was an error getting this.")
	} else {
		if check.Ok {
			msg.WriteString("Banned")
		} else {
			msg.WriteString("Not banned")
		}
		msg.WriteString(fmt.Sprintf(" (%s)", check.Description))
	}
	msg.WriteString("\n\tWallet: ")
	msg.WriteString(user.Wallet)
	msg.WriteString(fmt.Sprintf("\nType `/approve %d` to approve this user.", user.TelegramID))

	approvalMessage, err := hm.Bot.Send(hm.AdminGroup, msg.String())
	if err != nil {
		logrus.Error(err)
		hm.Bot.Send(user.TBUser(), "There was an error requesting payout approval.")
		return
	}

	mID, cID := approvalMessage.MessageSig()
	stored := &tb.StoredMessage{
		MessageID: mID,
		ChatID:    cID,
	}
	ok = hm.Users.AddApproval(user.TelegramID, "join", stored)
	if !ok {
		logrus.Error("Was unable to store approval message for updating.")
		return
	}
	hm.Bot.Send(user.TBUser(), "I've sent your request in for approval! You'll get your reward once an admin confirms it.")
}
