package handlers

import (
	"fmt"
	"strings"

	tb "gopkg.in/tucnak/telebot.v2"
)

// List lists users pending an approval.
func (hm *HandlerManager) List(m *tb.Message) {
	hm.Usage.Report.CommandUsage.List++
	approvals := hm.Users.ListApprovals()
	var response strings.Builder
	var approvalAllMessage strings.Builder
	if len(approvals) == 0 {
		response.WriteString("There are no users waiting to be approved.")
	} else {
		response.WriteString("The following users are waiting to have their reward approved:")
		approvalAllMessage.WriteString("To approve all of them, use:\n/approve")
		for _, userID := range approvals {
			response.WriteString(fmt.Sprintf("\n\t%d (`/approve %d`)", userID, userID))
			approvalAllMessage.WriteString(fmt.Sprintf(" %d", userID))
		}
	}
	hm.Bot.Send(m.Chat, response.String())
	if len(approvals) > 1 {
		hm.Bot.Send(m.Chat, approvalAllMessage.String())
	}
}
