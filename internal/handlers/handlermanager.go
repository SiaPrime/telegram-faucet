package handlers

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/SiaPrime/telegram-faucet/internal/users"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (hm *HandlerManager) inGroup(u *users.User) bool {
	chatMember, err := hm.Bot.ChatMemberOf(hm.TrackedGroup, u.TBUser())
	if err != nil {
		logrus.Error(err)
		return false
	}
	if chatMember.Role == "restricted" ||
		chatMember.Role == "left" ||
		chatMember.Role == "kicked" {
		return false
	}
	return true
}

func (hm *HandlerManager) rewardJoin(userID int, username string) bool {
	hm.RewardMtx.Lock()
	defer hm.RewardMtx.Unlock()
	user, ok := hm.Users.Load(userID)
	if !ok {
		hm.adminError("Illegal state. Attempted to reward uncreated user.")
		return false
	}
	if user.HasJoinReward() {
		hm.adminError(fmt.Sprintf("User '%s' has already received their join reward.", username))
		return false
	}
	res, err := hm.Sia.Send(hm.JoinReward, user.Wallet)
	if err != nil || len(res.Transactionids) == 0 {
		hm.adminError(fmt.Sprintf("Error providing join reward to '%s' (id: %d)", username, userID))
		if err != nil {
			logrus.Error(err)
		} else {
			hm.adminError("The daemon endpoint did not return any transactions")
		}
		_, err := hm.Bot.Send(user.TBUser(), "There was an error rewarding you for joining. Please reach out to a group admin. We'll fix this up.")
		if err != nil {
			logrus.Errorf("Failed to send to user '%s' (id: %d) with fake TBUser", username, userID)
		}
		return false
	}
	hm.Users.SetJoinReward(user.TelegramID, res.Transactionids)
	_, err = hm.Bot.Send(user.TBUser(), "You have been rewarded for joining, I hope you enjoy the group!")
	if err != nil {
		logrus.Errorf("Failed to send to user '%s' (id: %d) with fake TBUser", username, userID)
	}
	hm.rewardReferrer(user, true)
	return true
}

func (hm *HandlerManager) adminError(msg string) {
	logrus.Error(msg)
	hm.Bot.Send(hm.AdminGroup, msg)
}

// rewardReferrer should only be called after verifying the user has not received a join reward
// as we only reward on registering and joining the first time, should only be called from
// rewardJoin (or a method holding the reward lock)
func (hm *HandlerManager) rewardReferrer(referredUser *users.User, notifyUsers bool) bool {
	if !referredUser.HasReferrer() {
		return false
	}
	if referredUser.TelegramID == referredUser.ReferrerID {
		logrus.Errorf("User %d tried to refer themself and was set as their referrer but not rewarded.", referredUser.TelegramID)
		return false
	}
	user, ok := hm.Users.Load(referredUser.ReferrerID)
	if !ok {
		logrus.Errorf("Was unable to locate referrer %d", referredUser.ReferrerID)
		return false
	}
	if !hm.inGroup(user) || !user.IsRegistered() {
		logrus.Infof("Did not reward user %d because they were determined to not meet the required conditions.", user.TelegramID)
		return false
	}
	res, err := hm.Sia.Send(hm.ReferralReward, user.Wallet)
	if err != nil {
		logrus.Errorf("Error providing referral reward to '%d'", user.TelegramID)
		logrus.Error(err)
		if notifyUsers {
			_, err2 := hm.Bot.Send(user.TBUser(), "There was an error rewarding you a referral. Please reach out to an admin and we'll fix this up.")
			if err2 != nil {
				logrus.Errorf("We were unable to send a message to user '%d' using a manually created user object.", user.TelegramID)
				logrus.Error(err2)
			}
		}
		return false
	}
	hm.Users.AddReferralReward(user.TelegramID, referredUser.TelegramID, res.Transactionids)
	if notifyUsers {
		_, err = hm.Bot.Send(referredUser.TBUser(), "We have rewarded your referrer!")
		if err != nil {
			logrus.Errorf("We were unable to send a message to user '%d' using a manually created user object.", user.TelegramID)
			logrus.Error(err)
		}
		_, err = hm.Bot.Send(user.TBUser(), "You have received a reward for referring another user.")
		if err != nil {
			logrus.Errorf("We were unable to send a message to user '%d' using a manually created user object.", user.TelegramID)
			logrus.Error(err)
		}
	}
	return true
}

// StartUsageBackup starts a backup thread for periodically saving the usage to disk
func (hm *HandlerManager) StartUsageBackup(shutdown <-chan int, done chan<- bool) {
	for {
		select {
		case <-shutdown:
			logrus.Info("Stopping usage backup thread.")
			hm.Usage.StoreUsage()
			done <- true
			return
		case <-time.After(15 * time.Minute):
			hm.UpdateUsage()
			hm.Usage.StoreUsage()
		}
	}
}

// EnforcePrivate returns true if the message is private and false if not. It also tells the user
// it must be used in a private message.
func (hm *HandlerManager) EnforcePrivate(m *tb.Message) bool {
	// if !m.Private() {
	// 	hm.Bot.Send(m.Chat, "That command can only be used in a private chat. Start a conversation with me and we'll get everything working.")
	// }
	if m.Sender.IsBot {
		logrus.Errorf("A bot (name: %s, id: %d) tried to interact with our bot.", m.Sender.Username, m.Sender.ID)
	}
	return m.Private() && !m.Sender.IsBot
}
