package handlers

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/SiaPrime/telegram-faucet/internal/util"

	"gitlab.com/SiaPrime/telegram-faucet/pkg/coingecko"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/sia"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Stats provides consensus and market data
func (hm *HandlerManager) Stats(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Stats++
	status, err := hm.CG.Status()
	if err != nil {
		logrus.Error(err)
		return
	}
	consensus, err := hm.Sia.Consensus()
	if err != nil {
		logrus.Error(err)
		return
	}
	hm.Bot.Send(m.Chat, buildStats(&consensus, &status))
}

func buildStats(consensus *sia.ConsensusResponse, status *coingecko.CoinStatus) string {
	var stats strings.Builder
	stats.WriteString(fmt.Sprintf("Consensus data for %s:", status.Name))
	stats.WriteString(fmt.Sprintf("\n\tBlock height: %s", util.I(consensus.Height)))
	stats.WriteString(fmt.Sprintf("\n\tDifficulty: %s", util.PrintHash(consensus.Difficulty)))
	stats.WriteString(fmt.Sprintf("\n\nMarket data for %s:", status.Name))
	stats.WriteString(fmt.Sprintf("\n\tPrice (USD): %s", status.PriceUSD))
	stats.WriteString(fmt.Sprintf("\n\tPrice (BTC): %s", status.PriceBTC))
	stats.WriteString(fmt.Sprintf("\n\n\tMarket Cap (USD): %s", status.MarketCapUSD))
	stats.WriteString(fmt.Sprintf("\n\t24h High (USD): %s", status.High24H))
	stats.WriteString(fmt.Sprintf("\n\t24h Low (USD): %s", status.Low24H))
	stats.WriteString(fmt.Sprintf("\n\tSupply: %s", status.Supply))
	stats.WriteString(fmt.Sprintf("\n\n\tChange over 24 hours: %s", status.PriceChange24H))
	stats.WriteString(fmt.Sprintf("\n\tChange over 7 days: %s", status.PriceChange7D))
	stats.WriteString(fmt.Sprintf("\n\tChange over 30 days: %s", status.PriceChange30D))
	return stats.String()
}
