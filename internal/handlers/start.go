package handlers

import (
	"fmt"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

var DISABLED_MESSAGE = "Our faucet is currently dry, please try again later"
var DISABLED_MESSAGE_INVITES = "Our faucet is currently dry, please try again later. Invites may be rewarded if it is re-enabled."

// Start handles the /start <PAYLOAD> telegram command
func (hm *HandlerManager) Start(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Start++
	user, loaded := hm.Users.LoadOrCreate(m.Sender.ID)
	if loaded {
		if user.HasPendingApproval() {
			hm.Bot.Send(m.Sender, "We're currently processing your approval. Please reach out in the group if you have any questions.")
			return
		} else if user.HasJoinReward() {
			hm.Bot.Send(user.TBUser(), "It looks like you have already joined, thanks for doing that!")
			return
		} else if user.HasReferrer() {
			if !hm.Disabled {
				hm.StartCheckStatus(m)
			} else {
				hm.Bot.Send(m.Sender, DISABLED_MESSAGE)
				return
			}
		}
	}
	if hm.Disabled {
		hm.Bot.Send(m.Sender, DISABLED_MESSAGE)
		return
	}
	// The only valid payload is a referrer code
	if m.Payload == "" {
		// TODO: Ask if they have a referral code with reply keys
		replyKeys := [][]tb.ReplyButton{
			[]tb.ReplyButton{startHaveReferral},
			[]tb.ReplyButton{startNoReferral},
		}
		hm.Bot.Send(m.Sender, "Do you have a referral code?", &tb.ReplyMarkup{
			ReplyKeyboard:   replyKeys,
			OneTimeKeyboard: true,
		})
	} else {
		hm.Usage.Report.CommandUsage.StartWithPayload++
		hm.referInternal(m)
	}
}

// RegisterStartButtons registers buttons /
func (hm *HandlerManager) RegisterStartButtons() {
	hm.Bot.Handle(&startHaveReferral, func(m *tb.Message) {
		if !m.Private() {
			return
		}
		hm.Bot.Send(m.Sender, "Thanks for letting me know!")
		hm.Bot.Send(m.Sender, "Please type `/refer <code>` to make sure you give credit to the person who invited you.")
	})

	hm.Bot.Handle(&startNoReferral, func(m *tb.Message) {
		if !m.Private() {
			return
		}
		hm.Bot.Send(m.Sender, "Thanks for letting me know!")
		hm.StartCheckStatus(m)
	})

	hm.Bot.Handle(&startHaveWallet, func(m *tb.Message) {
		if !m.Private() {
			return
		}
		hm.Bot.Send(m.Sender, "Thanks for letting me know!")
		hm.Bot.Send(m.Sender, "Please type `/register <address>` to associate the wallet address with your account.")
	})

	hm.Bot.Handle(&startNoWallet, func(m *tb.Message) {
		if !m.Private() {
			return
		}
		hm.Bot.Send(m.Sender, "Thanks for letting me know!")
		hm.Bot.Send(m.Sender, "Please read https://medium.com/@gadaboy11/getting-started-with-the-scprime-ui-b2e26ebd02f5 for instructions on how to get started!")
		hm.Bot.Send(m.Sender, "Once you've set up a wallet, come back here and type `/register <address>` to associate the wallet address with your account.")
	})
}

// StartCheckStatus moves the user on to the joined check
func (hm *HandlerManager) StartCheckStatus(m *tb.Message) {
	user, _ := hm.Users.LoadOrCreate(m.Sender.ID)
	if user.HasPendingApproval() {
		hm.Bot.Send(m.Sender, "We're currently processing your approval. Please reach out in the group if you have any questions.")
		return
	}
	if hm.Disabled {
		hm.Bot.Send(m.Sender, DISABLED_MESSAGE)
		return
	}
	if !user.IsRegistered() {
		replyKeys := [][]tb.ReplyButton{
			[]tb.ReplyButton{startHaveWallet},
			[]tb.ReplyButton{startNoWallet},
		}
		hm.Bot.Send(m.Sender, "You will need a wallet in order to receive rewards.")
		hm.Bot.Send(m.Sender, "Do you have a wallet to receive ScPrime?", &tb.ReplyMarkup{
			ReplyKeyboard:   replyKeys,
			OneTimeKeyboard: true,
		})
		return
	}
	if hm.inGroup(user) {
		if user.HasJoinReward() {
			logrus.Infof("User '%s' attempted to join a second time", m.Sender.Username)
			hm.Bot.Send(m.Sender, "My records indicate you have already been rewarded for joining.")
			return
		}
		hm.sendJoinApprovalMessage(m.Sender.ID)
		return
	}
	hm.Bot.Send(m.Sender,
		fmt.Sprintf("There is just one more step. You'll need to join our group to get your reward. Please click here to do so: %s", hm.TrackedGroupInviteLink))
}

var startHaveReferral = tb.ReplyButton{
	Text: "I have a referral code.",
}

var startNoReferral = tb.ReplyButton{
	Text: "I do not have a referral code.",
}

var startHaveWallet = tb.ReplyButton{
	Text: "I have a wallet address.",
}

var startNoWallet = tb.ReplyButton{
	Text: "I do not have a wallet.",
}

var startJoinedGroup = tb.ReplyButton{
	Text: "I have joined the group!",
}
