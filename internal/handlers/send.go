package handlers

import (
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Send sends currency to a specific wallet
func (hm *HandlerManager) Send(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Send++
	fields := strings.Fields(m.Text)
	if len(fields) != 3 {
		hm.Bot.Send(m.Chat, "Usage is /send <amount> <wallet>")
		return
	}
	amount, err := strconv.ParseUint(fields[1], 10, 64)
	if err != nil {
		logrus.Error(err.Error())
		hm.Bot.Send(m.Chat, "Failed!")
		return
	}
	wallet := fields[2]
	transactions, err := hm.Sia.Send(amount, wallet)
	if err != nil {
		logrus.Error(err.Error())
		hm.Bot.Send(m.Chat, "Failed!")
		return
	}
	hm.Bot.Send(m.Chat, "Sent!")
	logrus.Info(transactions)
}
