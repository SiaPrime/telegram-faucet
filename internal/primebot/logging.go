package primebot

import (
	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

func setupLogging() {
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "01/02 15:04:05"
	customFormatter.FullTimestamp = true
	logrus.SetFormatter(customFormatter)
}

func logGroup(b *tb.Bot, text string, id string) {
	chat, err := b.ChatByID(id)
	if err != nil {
		logrus.Errorf("Failed to load group (%s): %s", config.TrackedGroupID, err.Error())
	} else {
		logrus.Infof("%s: %s (id: %s)", text, chat.Title, config.TrackedGroupID)
	}
}
