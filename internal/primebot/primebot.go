package primebot

import (
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/SiaPrime/telegram-faucet/internal/handlers"
	"gitlab.com/SiaPrime/telegram-faucet/internal/usage"

	"gitlab.com/SiaPrime/telegram-faucet/internal/users"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/coingecko"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/sia"
	tb "gopkg.in/tucnak/telebot.v2"
)

var config Config

// Run runs the bot
func Run() {
	setupLogging()
	logrus.Info("Starting up Primebot.")
	config = loadConfig("primebot.json")

	logrus.Info("Loading users...")
	userStore := users.Users{
		Path: config.UsersPath,
	}
	loaded, err := userStore.LoadUsers()
	if err != nil {
		logrus.Error("Failed to load users!")
	}
	logrus.Infof("Loaded %d users.", loaded)

	usageStore := usage.Usage{
		Path: config.UsagePath,
	}
	usageStore.LoadUsage()

	logrus.Info("Starting bot.")
	b, err := tb.NewBot(tb.Settings{
		Token:  config.TelegramToken,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		logrus.Fatal(err)
	}
	logGroup(b, "Monitoring group", config.TrackedGroupID)
	trackedGroup, err := b.ChatByID(config.TrackedGroupID)
	if err != nil {
		logrus.Error(err)
	}
	logGroup(b, "Monitoring group", config.AdminGroupID)
	adminGroup, err := b.ChatByID(config.AdminGroupID)
	if err != nil {
		logrus.Error(err)
	}
	hm := handlers.HandlerManager{
		Disabled:               config.Disabled,
		Bot:                    b,
		CG:                     coingecko.NewSingleCoin(config.Coingecko),
		Sia:                    sia.New(config.Sia.Address, config.Sia.Password, config.Sia.UserAgent),
		Users:                  &userStore,
		UserReferralLink:       config.UserReferralLink,
		TrackedGroup:           trackedGroup,
		TrackedGroupInviteLink: config.TrackedGroupInviteLink,
		AdminGroup:             adminGroup,
		JoinReward:             config.Rewards.Join,
		ReferralReward:         config.Rewards.Referral,
		Usage:                  &usageStore,
	}
	registerHandlers(&hm)
	if hm.Disabled {
		logrus.Error("The bot is starting in disabled mode. Set 'disabled: false' in the config to enable it.")
		hm.Bot.Send(hm.AdminGroup, "The bot has started in disabled mode. New approval requests will not be accepted.")
	}
	go b.Start()

	shutdown := make(chan int)
	done := make(chan bool)
	workers := 0

	logrus.Info("Starting user backup thread.")
	go userStore.StartUsersBackup(shutdown, done)
	workers++

	logrus.Info("Starting usage backup thread.")
	go hm.StartUsageBackup(shutdown, done)
	workers++

	// Wait here until CTRL-C or other term signal is received.
	logrus.Info("PrimeBot is now running.  Press CTRL-C to exit.")
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sig

	logrus.Info("Shutting down.")
	close(shutdown)
	b.Stop() // Gracefully kill the telegram bot
	for i := 0; i < workers; i++ {
		<-done // Wait for the other threads to shut down
	}
	logrus.Info("Shutdown complete.")
}

func registerHandlers(hm *handlers.HandlerManager) {
	// Event Handlers
	hm.Bot.Handle(tb.OnAddedToGroup, func(m *tb.Message) {
		hm.OnAddedToGroup(m)
	})

	hm.Bot.Handle(tb.OnUserJoined, func(m *tb.Message) {
		hm.OnUserJoined(m)
	})

	// Command Handlers
	hm.RegisterStartButtons()
	hm.Bot.Handle("/start", func(m *tb.Message) {
		if hm.EnforcePrivate(m) {
			hm.Start(m)
		}
	})

	hm.Bot.Handle("/refer", func(m *tb.Message) {
		if hm.EnforcePrivate(m) {
			hm.Refer(m)
		}
	})

	hm.Bot.Handle("/register", func(m *tb.Message) {
		if hm.EnforcePrivate(m) {
			hm.Register(m)
		}
	})

	hm.Bot.Handle("/invite", func(m *tb.Message) {
		if hm.EnforcePrivate(m) {
			hm.Invite(m)
		}
	})

	hm.Bot.Handle("/stats", func(m *tb.Message) {
		hm.Stats(m)
	})

	/*
	 *	Restricted Commands
	 */
	// Command: /id
	// Purpose: Provide user and group ID information for configuring the bot.
	hm.Bot.Handle("/id", func(m *tb.Message) {
		if len(config.AdminIDs) == 0 || isAdmin(m) {
			hm.ID(m)
		}
	})

	// Command: /send
	// Purpose: Allow admins to send currency to a registered user
	hm.Bot.Handle("/send", func(m *tb.Message) {
		if isAdmin(m) {
			hm.Send(m)
		}
	})

	// Command: /balance
	// Purpose: Check the balance of the wallet
	hm.Bot.Handle("/balance", func(m *tb.Message) {
		if isAdmin(m) {
			hm.Balance(m)
		}
	})

	// Command: /report
	// Purpose: Provide a report on the tracked group.
	hm.Bot.Handle("/report", func(m *tb.Message) {
		if isAdmin(m) {
			hm.Report(m)
		}
	})

	// Command: /approve <id>
	// Purpose: Approve a user and their referrer for rewards
	hm.Bot.Handle("/approve", func(m *tb.Message) {
		if isAdmin(m) && m.Chat.ID == hm.AdminGroup.ID {
			hm.Approve(m)
		}
	})

	// Command: /approveAll <id>
	// Purpose: Approve all users and their referrers for rewards
	hm.Bot.Handle("/approveAll", func(m *tb.Message) {
		if isAdmin(m) && m.Chat.ID == hm.AdminGroup.ID {
			hm.ApproveAll(m)
		}
	})

	// Command: /list
	// Purpose: List all users pending approvalreferringUser
	hm.Bot.Handle("/list", func(m *tb.Message) {
		if isAdmin(m) && m.Chat.ID == hm.AdminGroup.ID {
			hm.List(m)
		}
	})

	// Command: /fixReferrals
	// Purpose: List all users pending approvalreferringUser
	hm.Bot.Handle("/fixReferrals", func(m *tb.Message) {
		if isAdmin(m) && m.Chat.ID == hm.AdminGroup.ID {
			hm.FixReferrals(m)
		}
	})

	// Command: /dump
	// Purpose: Write a file with owed ballances
	hm.Bot.Handle("/dump", func(m *tb.Message) {
		if isAdmin(m) && m.Chat.ID == hm.AdminGroup.ID {
			hm.Dump()
		}
	})
}

func isAdmin(m *tb.Message) bool {
	userID := strconv.Itoa(m.Sender.ID)
	logrus.Debugf("Checking admin for userID: %s", userID)
	for _, adminID := range config.AdminIDs {
		logrus.Debugf("Against ID: %s", adminID)
		if adminID == userID {
			return true
		}
	}
	logrus.Errorf("User '%s' (id: %s) attempted to use `%s`, but is not an admin.", m.Sender.Username, userID, m.Text)
	return false
}
